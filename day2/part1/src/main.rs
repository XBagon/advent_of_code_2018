#![feature(test)]
extern crate test;

use std::io::{stdin, BufRead};

fn main() {
    println!("{}", logic(stdin().lock().lines()));
}

fn logic<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: U) -> usize {
    let (twices, thrices) = lines
        .map(|l| {
            l.unwrap().bytes().fold([0u8; 26], |mut occs, c| {
                occs[(c - 97) as usize] += 1;
                occs
            })
        })
        .fold((0usize, 0usize), |(mut tw_occs, mut thr_occs), occs| {
            let (twice, thrice) = occs.iter().fold((false, false), |(mut twice, mut thrice), c_occ| {
                match c_occ {
                    2 => twice = true,
                    3 => thrice = true,
                    _ => {}
                }
                (twice, thrice)
            });
            if twice {
                tw_occs += 1;
            }
            if thrice {
                thr_occs += 1;
            }
            (tw_occs, thr_occs)
        });
    twices * thrices
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day2_1() {
        assert_eq!(logic(include_bytes!("../../input").lines()), 5658);
    }

    #[bench]
    fn bench_day2_1(b: &mut test::Bencher) {
        b.iter(|| logic(include_bytes!("../../input").lines()));
    }
}
