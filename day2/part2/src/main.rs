#![feature(test)]
extern crate test;

use helper::exit;
use std::io::{stdin, BufRead};

fn main() {
    println!("{}", logic(stdin().lock().lines()));
}

fn logic<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: U) -> String {
    let mut lines_vec: Vec<String> = Vec::new();
    for l0 in lines {
        let l0 = l0.unwrap();
        for l1 in lines_vec.iter() {
            if let Some(in_com) = one_in_common(&l0, &l1) {
                return in_com;
            }
        }
        lines_vec.push(l0);
    }

    exit!("No matching IDs found")
}

pub fn one_in_common(a: &str, b: &str) -> Option<String> {
    let mut dist_one = false;
    for (c0, c1) in a.chars().zip(b.chars()) {
        if c0 != c1 {
            if !dist_one {
                dist_one = true
            } else {
                return None;
            }
        }
    }
    Some(
        a.chars()
            .zip(b.chars())
            .filter_map(|(c0, c1)| {
                if c0 == c1 {
                    Some(c0)
                } else {
                    None
                }
            })
            .collect(),
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day2_2() {
        assert_eq!(logic(include_bytes!("../../input").lines()), "nmgyjkpruszlbaqwficavxneo");
    }

    #[bench]
    fn bench_day2_2(b: &mut test::Bencher) {
        b.iter(|| logic(include_bytes!("../../input").lines()));
    }
}
