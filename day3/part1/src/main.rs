#![feature(test)]
#![allow(clippy::cast_lossless, clippy::float_cmp)]
extern crate test;

use cgmath::Point2;
use collision::{
    dbvt::{DiscreteVisitor, DynamicBoundingVolumeTree, TreeValue},
    Aabb2, SurfaceArea,
};
use itertools::Itertools;
use regex::Regex;
use std::io::{stdin, BufRead};

fn main() {
    println!("{}", logic(stdin().lock().lines()));
}

fn logic<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: U) -> usize {
    let rgx = Regex::new(r"#\d* @ (\d*),(\d*): (\d*)x(\d*)").unwrap();
    let mut claimed = RectSet::new();
    let mut overlapped = RectSet::new();
    for line in lines {
        let line = line.unwrap();
        let captures = rgx.captures(&line).unwrap();
        let pos_x = captures[1].parse().unwrap();
        let pos_y = captures[2].parse().unwrap();
        let new = Rect::new(Aabb2::<f32> {
            min: Point2::new(pos_x, pos_y),
            max: Point2::new(
                pos_x + captures[3].parse::<u16>().unwrap() as f32,
                pos_y + captures[4].parse::<u16>().unwrap() as f32,
            ),
        });
        overlapped.add(claimed.intersections(&new));
        claimed.add_rect(new);
    }
    overlapped.rects.values().iter().map(|(_, r)| r.bounds.surface_area()).sum::<f32>() as usize
}

#[derive(Clone, Debug)]
struct Rect {
    bounds: Aabb2<f32>,
}

impl Rect {
    fn new(bounds: Aabb2<f32>) -> Self {
        Rect {
            bounds,
        }
    }

    fn covers(&self, other: &Self) -> bool {
        self.bounds.min.x < other.bounds.min.x
            && self.bounds.max.x > other.bounds.max.x
            && self.bounds.min.y < other.bounds.min.y
            && self.bounds.max.y > other.bounds.max.y
    }

    fn intersection(&self, other: &Self) -> Rect {
        let mut intersection = Rect {
            bounds: Aabb2::new(Point2::new(0.0, 0.0), Point2::new(0.0, 0.0)),
        };
        if self.bounds.min.x < other.bounds.min.x {
            intersection.bounds.min.x = other.bounds.min.x;
        } else {
            intersection.bounds.min.x = self.bounds.min.x;
        }
        if self.bounds.max.x < other.bounds.max.x {
            intersection.bounds.max.x = self.bounds.max.x;
        } else {
            intersection.bounds.max.x = other.bounds.max.x;
        }
        if self.bounds.min.y < other.bounds.min.y {
            intersection.bounds.min.y = other.bounds.min.y;
        } else {
            intersection.bounds.min.y = self.bounds.min.y;
        }
        if self.bounds.max.y < other.bounds.max.y {
            intersection.bounds.max.y = self.bounds.max.y;
        } else {
            intersection.bounds.max.y = other.bounds.max.y;
        }
        intersection
    }

    fn intersects(&self, other: &Self) -> bool {
        !(self.bounds.max.x <= other.bounds.min.x
            || other.bounds.max.x <= self.bounds.min.x
            || self.bounds.max.y <= other.bounds.min.y
            || other.bounds.max.y <= self.bounds.min.y)
    }

    fn difference(&self, other: &Self) -> Vec<Rect> {
        let mut differences = Vec::new();
        if self.intersects(other) {
            if self.bounds.min.x < other.bounds.min.x {
                differences.push(Rect {
                    bounds: Aabb2::new(
                        Point2::new(self.bounds.min.x, self.bounds.min.y),
                        Point2::new(other.bounds.min.x, self.bounds.max.y),
                    ),
                });
            }
            if self.bounds.max.x > other.bounds.max.x {
                differences.push(Rect {
                    bounds: Aabb2::new(
                        Point2::new(other.bounds.max.x, self.bounds.min.y),
                        Point2::new(self.bounds.max.x, self.bounds.max.y),
                    ),
                });
            }
            if self.bounds.min.y < other.bounds.min.y {
                differences.push(Rect {
                    bounds: Aabb2::new(
                        Point2::new(other.bounds.min.x.max(self.bounds.min.x), self.bounds.min.y),
                        Point2::new(other.bounds.max.x.min(self.bounds.max.x), other.bounds.min.y),
                    ),
                });
            }
            if self.bounds.max.y > other.bounds.max.y {
                differences.push(Rect {
                    bounds: Aabb2::new(
                        Point2::new(other.bounds.min.x.max(self.bounds.min.x), other.bounds.max.y),
                        Point2::new(other.bounds.max.x.min(self.bounds.max.x), self.bounds.max.y),
                    ),
                });
            }
        } else {
            differences.push(self.clone());
        }
        differences
    }
}

impl TreeValue for Rect {
    type Bound = Aabb2<f32>;

    fn bound(&self) -> &<Self as TreeValue>::Bound {
        &self.bounds
    }

    fn get_bound_with_margin(&self) -> <Self as TreeValue>::Bound {
        self.bounds
    }
}

struct RectSet {
    rects: DynamicBoundingVolumeTree<Rect>,
}

impl RectSet {
    fn new() -> Self {
        RectSet {
            rects: DynamicBoundingVolumeTree::new(),
        }
    }

    fn intersections(&mut self, rect: &Rect) -> Self {
        let mut intersections = RectSet::new();
        let mut visitor = DiscreteVisitor::<_, Rect>::new(&rect.bounds);
        for (touching_index, _) in self.rects.query_for_indices(&mut visitor) {
            let (_, overlapping) = &self.rects.values()[touching_index];
            intersections.add_rect(rect.intersection(overlapping));
        }
        intersections
    }

    fn add(&mut self, mut new: Self) {
        for (_, new_rect) in new.rects.values_mut().drain(..) {
            self.add_rect(new_rect);
        }
    }

    fn add_rect(&mut self, new_rect: Rect) {
        let mut visitor = DiscreteVisitor::<_, Rect>::new(&new_rect.bounds);
        let mut to_add = vec![new_rect.clone()]; //TODO: is clone necessary?
        let overlapping_rects = self.rects.query_for_indices(&mut visitor);
        {
            let old_rects = self.rects.values();
            if overlapping_rects.len() == 1 && old_rects[overlapping_rects[0].0].1.covers(&new_rect) {
                //already covered
                return;
            }
            for (overlapping_index, _) in overlapping_rects {
                let (_, overlapping) = &old_rects[overlapping_index];
                let mut finer = Vec::new();
                for rect in to_add {
                    finer.append(&mut rect.difference(overlapping));
                }
                to_add = finer;
            }
        }
        'outer: for add in to_add {
            let bounds = Aabb2::new(
                Point2::new(add.bounds.min.x - 1.0, add.bounds.min.y - 1.0),
                Point2::new(add.bounds.max.x + 1.0, add.bounds.max.y + 1.0),
            );
            let mut visitor = DiscreteVisitor::<_, Rect>::new(&bounds);
            let neighbors = self.rects.query_for_indices(&mut visitor);
            if neighbors.len() == 4 {
                //check if rect covers hole
                let sorted_by_min_x;
                let sorted_by_min_y;
                let sorted_by_max_x;
                let sorted_by_max_y;
                let neighbor_rects: Vec<&Rect> =
                    neighbors.iter().map(|(index, _)| &self.rects.values()[*index].1).collect();
                let neighbor_rects_iter = neighbor_rects.iter();
                sorted_by_min_x =
                    neighbor_rects_iter.clone().map(|r| r.bounds.min.x).sorted_by(|f0, f1| f0.partial_cmp(f1).unwrap());
                sorted_by_min_y =
                    neighbor_rects_iter.clone().map(|r| r.bounds.min.y).sorted_by(|f0, f1| f0.partial_cmp(f1).unwrap());
                sorted_by_max_x =
                    neighbor_rects_iter.clone().map(|r| r.bounds.max.x).sorted_by(|f0, f1| f0.partial_cmp(f1).unwrap());
                sorted_by_max_y =
                    neighbor_rects_iter.clone().map(|r| r.bounds.max.y).sorted_by(|f0, f1| f0.partial_cmp(f1).unwrap());
                if add.bounds.min.x == sorted_by_min_x[3]
                    && add.bounds.min.y == sorted_by_min_y[3]
                    && add.bounds.max.x == sorted_by_max_x[0]
                    && add.bounds.max.y == sorted_by_max_y[0]
                {
                    let center_rect = Rect::new(Aabb2::new(
                        Point2::new(sorted_by_min_x[1], sorted_by_min_y[1]),
                        Point2::new(sorted_by_max_x[2], sorted_by_max_y[2]),
                    ));
                    let side_rects: Vec<_> =
                        <_ as Iterator>::flatten(neighbor_rects_iter.map(|r| r.difference(&center_rect))).collect();
                    self.add_rect(center_rect);
                    for rect in side_rects {
                        self.add_rect(rect);
                    }
                    for (i, _) in neighbors {
                        self.rects.remove(i);
                    }
                    self.rects.do_refit();
                    continue 'outer;
                }
            }
            for (neighbor_index, _) in neighbors {
                let (node_id, neighbor) = &mut self.rects.values_mut()[neighbor_index];
                if add.bounds.min.x == neighbor.bounds.min.x && add.bounds.max.x == neighbor.bounds.max.x {
                    if add.bounds.min.y < neighbor.bounds.min.y {
                        neighbor.bounds.min.y = add.bounds.min.y;
                    } else {
                        neighbor.bounds.max.y = add.bounds.max.y;
                    }
                    let node_id = *node_id;
                    self.rects.flag_updated(node_id);
                    self.rects.tick();
                    continue 'outer;
                } else if add.bounds.min.y == neighbor.bounds.min.y && add.bounds.max.y == neighbor.bounds.max.y {
                    if add.bounds.min.x < neighbor.bounds.min.x {
                        neighbor.bounds.min.x = add.bounds.min.x;
                    } else {
                        neighbor.bounds.max.x = add.bounds.max.x;
                    }
                    let node_id = *node_id;
                    self.rects.flag_updated(node_id);
                    self.rects.tick();
                    continue 'outer;
                }
            }
            self.rects.insert(add);
            self.rects.do_refit();
        }
    }

    #[allow(unused)]
    fn draw(&self) {
        let mut drawing = vec![255u8; 1000 * 1000];
        for (_, rect) in self.rects.values() {
            for y in rect.bounds.min.y as usize..rect.bounds.max.y as usize {
                for x in rect.bounds.min.x as usize..rect.bounds.max.x as usize {
                    drawing[y * 1000 + x] = 0;
                }
            }
        }
        image::GrayImage::from_vec(1000, 1000, drawing).unwrap().save("rect_set.png").unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day3_1() {
        assert_eq!(logic(include_bytes!("../../input").lines()), 105071);
    }

    #[bench]
    fn bench_day3_1(b: &mut test::Bencher) {
        b.iter(|| logic(include_bytes!("../../input").lines()));
    }
}
