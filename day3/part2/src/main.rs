#![feature(test)]
#![allow(clippy::cast_lossless, clippy::float_cmp)]
extern crate test;

use cgmath::Point2;
use collision::{
    dbvt::{DiscreteVisitor, DynamicBoundingVolumeTree, TreeValue},
    Aabb2,
};
use regex::Regex;
use std::io::{stdin, BufRead};

fn main() {
    println!("{}", logic(stdin().lock().lines()));
}

fn logic<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: U) -> usize {
    let rgx = Regex::new(r"#(\d*) @ (\d*),(\d*): (\d*)x(\d*)").unwrap();
    let mut claimed = RectSet::new();
    let mut overlapped_ids = Vec::new();
    for line in lines {
        let line = line.unwrap();
        let captures = rgx.captures(&line).unwrap();
        let id = captures[1].parse().unwrap();
        let pos_x = captures[2].parse().unwrap();
        let pos_y = captures[3].parse().unwrap();
        let new = Rect::new(
            id,
            Aabb2::<f32> {
                min: Point2::new(pos_x, pos_y),
                max: Point2::new(
                    pos_x + captures[4].parse::<u16>().unwrap() as f32,
                    pos_y + captures[5].parse::<u16>().unwrap() as f32,
                ),
            },
        );
        for id in claimed.intersection_ids(&new) {
            let extension = id as i64 - overlapped_ids.len() as i64;
            if extension > 0 {
                overlapped_ids.append(&mut vec![false; extension as usize]);
            }
            overlapped_ids[(id - 1) as usize] = true;
        }
        claimed.add_rect(new);
    }
    overlapped_ids.iter().position(|id| !*id).unwrap_or(overlapped_ids.len()) + 1
}

#[derive(Clone, Debug)]
struct Rect {
    id: u16,
    bounds: Aabb2<f32>,
}

impl Rect {
    fn new(id: u16, bounds: Aabb2<f32>) -> Self {
        Rect {
            id,
            bounds,
        }
    }

    fn intersects(&self, other: &Self) -> bool {
        !(self.bounds.max.x <= other.bounds.min.x
            || other.bounds.max.x <= self.bounds.min.x
            || self.bounds.max.y <= other.bounds.min.y
            || other.bounds.max.y <= self.bounds.min.y)
    }

    fn difference(&self, other: &Self) -> Vec<Rect> {
        let mut differences = Vec::new();
        if self.intersects(other) {
            if self.bounds.min.x < other.bounds.min.x {
                differences.push(Rect {
                    id: self.id,
                    bounds: Aabb2::new(
                        Point2::new(self.bounds.min.x, self.bounds.min.y),
                        Point2::new(other.bounds.min.x, self.bounds.max.y),
                    ),
                });
            }
            if self.bounds.max.x > other.bounds.max.x {
                differences.push(Rect {
                    id: self.id,
                    bounds: Aabb2::new(
                        Point2::new(other.bounds.max.x, self.bounds.min.y),
                        Point2::new(self.bounds.max.x, self.bounds.max.y),
                    ),
                });
            }
            if self.bounds.min.y < other.bounds.min.y {
                differences.push(Rect {
                    id: self.id,
                    bounds: Aabb2::new(
                        Point2::new(other.bounds.min.x.max(self.bounds.min.x), self.bounds.min.y),
                        Point2::new(other.bounds.max.x.min(self.bounds.max.x), other.bounds.min.y),
                    ),
                });
            }
            if self.bounds.max.y > other.bounds.max.y {
                differences.push(Rect {
                    id: self.id,
                    bounds: Aabb2::new(
                        Point2::new(other.bounds.min.x.max(self.bounds.min.x), other.bounds.max.y),
                        Point2::new(other.bounds.max.x.min(self.bounds.max.x), self.bounds.max.y),
                    ),
                });
            }
        } else {
            differences.push(self.clone());
        }
        differences
    }
}

impl TreeValue for Rect {
    type Bound = Aabb2<f32>;

    fn bound(&self) -> &<Self as TreeValue>::Bound {
        &self.bounds
    }

    fn get_bound_with_margin(&self) -> <Self as TreeValue>::Bound {
        self.bounds
    }
}

struct RectSet {
    rects: DynamicBoundingVolumeTree<Rect>,
}

impl RectSet {
    fn new() -> Self {
        RectSet {
            rects: DynamicBoundingVolumeTree::new(),
        }
    }

    fn intersection_ids(&mut self, rect: &Rect) -> Vec<u16> {
        let mut intersection_ids = Vec::new();
        let mut visitor = DiscreteVisitor::<_, Rect>::new(&rect.bounds);
        let intersecting = self.rects.query_for_indices(&mut visitor);
        if intersecting.len() == 0 {
            intersection_ids
        } else {
            for (touching_index, _) in intersecting {
                let (_, overlapping) = &self.rects.values()[touching_index];
                intersection_ids.push(overlapping.id);
            }
            intersection_ids.push(rect.id);
            intersection_ids
        }
    }

    fn add(&mut self, mut new: Self) {
        for (_, new_rect) in new.rects.values_mut().drain(..) {
            self.add_rect(new_rect);
        }
    }

    fn add_rect(&mut self, new_rect: Rect) {
        let mut visitor = DiscreteVisitor::<_, Rect>::new(&new_rect.bounds);
        let mut to_add = vec![new_rect.clone()]; //TODO: is clone necessary?
        let overlapping_rects = self.rects.query_for_indices(&mut visitor);
        {
            let old_rects = self.rects.values();
            for (overlapping_index, _) in overlapping_rects {
                let (_, overlapping) = &old_rects[overlapping_index];
                let mut finer = Vec::new();
                for rect in to_add {
                    finer.append(&mut rect.difference(overlapping));
                }
                to_add = finer;
            }
        }
        for add in to_add {
            self.rects.insert(add);
        }
        self.rects.do_refit();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day3_2() {
        assert_eq!(logic(include_bytes!("../../input").lines()), 222);
    }

    #[bench]
    fn bench_day3_2(b: &mut test::Bencher) {
        b.iter(|| logic(include_bytes!("../../input").lines()));
    }
}
