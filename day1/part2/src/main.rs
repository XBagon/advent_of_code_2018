#![feature(test)]
extern crate test;

use hashbrown::HashSet;
use helper::exit;
use itertools::{
    FoldWhile::{Continue, Done},
    Itertools,
};
use std::io::{stdin, BufRead};

fn main() {
    println!("{}", logic(stdin().lock().lines()));
}

fn logic<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: U) -> isize {
    let mut reached = HashSet::new();
    let mut changes = Vec::new();
    match lines.map(|l| l.unwrap().parse::<isize>().unwrap_or_else(|_| exit!("Failed parsing number"))).fold_while(
        0,
        |freq, change| {
            if !reached.insert(freq) {
                Done(freq)
            } else {
                changes.push(change);
                Continue(freq + change)
            }
        },
    ) {
        Done(twice) => twice,
        Continue(cont) => changes
            .into_iter()
            .cycle()
            .fold_while(cont, |freq, change| {
                if !reached.insert(freq) {
                    Done(freq)
                } else {
                    Continue(freq + change)
                }
            })
            .into_inner(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day1_2() {
        assert_eq!(logic(include_bytes!("../../input").lines()), 75749);
    }

    #[bench]
    fn bench_day1_2(b: &mut test::Bencher) {
        b.iter(|| logic(include_bytes!("../../input").lines()));
    }
}
