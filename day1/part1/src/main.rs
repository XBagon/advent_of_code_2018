#![feature(test)]
extern crate test;

use helper::exit;
use std::io::{stdin, BufRead};

fn main() {
    println!("{}", logic(stdin().lock().lines()));
}

fn logic<T: std::fmt::Debug, U: Iterator<Item = Result<String, T>>>(lines: U) -> isize {
    lines.map(|l| l.unwrap().parse::<isize>().unwrap_or_else(|_| exit!("Failed parsing number"))).sum::<isize>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day1_1() {
        assert_eq!(logic(include_bytes!("../../input").lines()), 525);
    }

    #[bench]
    fn bench_day1_1(b: &mut test::Bencher) {
        b.iter(|| logic(include_bytes!("../../input").lines()));
    }
}
